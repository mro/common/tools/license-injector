# Licensing script

Script can add license data from a given file to all source files with given extensions based on information from Git.

A sample license file `sample_license.txt` is provided which contains all available template fileds that can be replaced automatically per-file. 

If the license is already present (file contains word `Copyright`), the file will be skipped.

## Usage

1. Move `licenseInjector.sh` and license file to root folder of your repo
2. `chmod +x licenseInjector.sh`
3. `./licenseInjector.sh -f LICENSE_FILE -t EXTENSION_1 -c LINE_COMMENT_CHAR -s BLOCK_COMM_START -e BLOCK_COMM_END`


### Available arguments

- `-f` License file name (path relative to current dir)
- `-t` File extension (without dot), can be used multiple times
- `-c` Line comment character(s) (e.g. " \*" for C++)
- `-s` Block comment start delimiter (e.g. "/\*" for C++)
- `-e` Block comment end delimiter (e.g. " \*/" for C++)

Note: The line comment character is optional, and so are the block comment delimiters, but they both must be provided if at least one is. 

Note: No whitespace will be added before the comment characters or delimiters, so necessary spaces must be included (e.g. for C++ use " \*" and " \*/" instead of "\*" and "\*/").

### Examples

Add license to `.cpp` and `.h` files in current directory and all subdirs (all comment options used):
```
./licenseInjector.sh -f license.txt -t cpp -t h -c " *" -s "/*" -e " */"
```
Add license to `.py` files in current directory and all subdirs (no line comment chars):
```
./licenseInjector.sh -f license.txt -t py -s "'''" -e "'''"
```
Add license to `.robot` files in current directory and all subdirs (no block comments):
```
./licenseInjector.sh -f license.txt -t robot -c "#"
```
