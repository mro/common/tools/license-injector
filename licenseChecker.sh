#!/bin/bash

# Parse input args
while getopts t:d: params; do
	case "${params}" in
		t) fileExtensions+=(${OPTARG});;
		d) directories+=(${OPTARG});;
	esac
done
# Check that all required args provided
if [ ${#fileExtensions[*]} == 0 ]; then
	echo "No file extensions provided"
	exit 22
fi

# If no directories provided, add just current one
if [ ${#directories[*]} == 0 ]; then
	directories+="./"
fi

# Iterate over all files with give extension
# Or all files if none given
shopt -s globstar
missingLicense=false
for dir in ${directories[@]}; do
	for file in ${fileExtensions[@]/#/"${dir}"**/*.}; do
		[ -f "$file" ] || continue
		if grep -q Copyright "$file"; then continue; fi	

		missingLicense=true
		# Get Git data for file
		echo "No Copyright in file " $file
	done
done

if [ "$missingLicense" != "false" ]; then
	echo "Not all files have license header"
	exit 1
else
	echo "All licenses ok"
	exit 0
fi
