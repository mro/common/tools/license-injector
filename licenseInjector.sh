#!/bin/bash

# Parse input args
while getopts f:t:c:s:e: params; do
	case "${params}" in
		f) licenseFile=(${OPTARG});;
		t) fileExtensions+=(${OPTARG});;
		c) commentLine=(${OPTARG});;
		s) commentDelimStart=(${OPTARG});; 
		e) commentDelimEnd=(${OPTARG});;
	esac
done
# Check that all required args provided
if [ -z "$licenseFile" ]; then
	echo "No license file name provided"
	exit 22
fi
if [ ! -f "$licenseFile" ]; then
	echo "License file does not exist"
	exit 2
fi
if [ ${#fileExtensions[*]} == 0 ]; then
	echo "No file extensions provided"
	exit 22
fi
if [ -z "$commentDelimStart" ]; then
	if ! [ -z "$commentDelimEnd" ]; then
		echo "Provide also block comment start delimiter"
		exit 22
	fi
	sedBlockCommCmd=""
	commentDelimStart="d"
	commentDelimEnd="d"
else
	if [ -z "$commentDelimEnd" ]; then
		echo "Provide also block comment end delimiter"
		exit 22
	fi
	sedBlockCommCmd="s"
	commentDelimStart+="/"
	commentDelimEnd+="/"
fi

# Iterate over all files with give extension
# Or all files if none given
shopt -s globstar
for file in ${fileExtensions[@]/#/**/*.}; do
	[ -f "$file" ] || continue
	if grep -q Copyright "$file"; then continue; fi	

	# Get Git data for file	
	authorData=$(git log --format="format:%ad:%an:%ae" --date="short" --diff-filter=A $file)
	if ! [[ "$authorData" =~ ([[:digit:]]{4})-([[:digit:]]{2})-([[:digit:]]{2}):(.+):(.+) ]]; then
		echo "Failed to parse git log data - aborting"
		exit 131
	fi
	authYear="${BASH_REMATCH[1]}"
	authMonth="${BASH_REMATCH[2]}"
	authDay="${BASH_REMATCH[3]}"
	authName="${BASH_REMATCH[4]}"
	authEmail="${BASH_REMATCH[5]}"

	# Insert into file while replacing data
	sed \
	-e "s/@@YEAR@@/$authYear/" \
	-e "s/@@MONTH@@/$authMonth/" \
	-e "s/@@DAY@@/$authDay/" \
	-e "s/@@AUTHOR_NAME@@/$authName/" \
	-e "s/@@AUTHOR_EMAIL@@/$authEmail/" \
	-e "s/@@LINE_COMM@@/$commentLine/" \
	-e "$sedBlockCommCmd/@@BLOCK_COMMENT_START@@/$commentDelimStart" \
	-e "$sedBlockCommCmd/@@BLOCK_COMMENT_END@@/$commentDelimEnd" \
	$licenseFile | cat - $file > tempSubsFile
	mv tempSubsFile $file
done
